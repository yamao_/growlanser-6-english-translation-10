Visit https://growlanser6english.blogspot.com/ for more information.

[ONGOING] = This file still needs to be fully translated

[TRANSLATED] = This file has been translated and needs to be edited/proofread

[COMPLETED] = This file has been fully translated and edited (further edits may continue)
_____________________________________________________

CURRENTLY WORKING ON:

1. mako - Gem Properties Abbreviations/Descriptions (read: procrastinating)
2. risae - Translating the GL6 ELF File, inserting the Growlanser Realm script, testing the scripts, finding GL5 VWF code



CURRENT TO-DO LIST:

1. Implement the Gem Properties Abbreviations/Descriptions workaround --- ONGOING
2. Translate the remaining TIM2 files --- ONGOING
3. Adding subtitles to .mpg movie files --- ONGOING
4. Figuring out how to change the standard name in the 2nd naming window (Haschens naming window) --- ONGOING
5. Maybe figuring out how to expand .FLK script files? --- ONGOING
6. Translate the going-into-a-city-name files, which i believe are image files --- ONGOING



CURRENT ROADBLOCKS:

1. Importing GL5's VWF and .fnt font into GL6
2. Reverse engineering the ELF file to allow for better translations



CURRENT PROGRESS:

1. Menus (minus gem abbrev./desc.) --- DONE
2. SCEN scripts ---
	- prologue --- DONE
	- chapter 1 --- DONE
3. Inserting Growlanser Realms script --- 25%
4. GL6 MOV --- 0/XX
5. tm2 texture files ---
6. ELF file --- 85%
7. Updating the GL6 .fnt font to a better font --- ONGOING
8. Extract scene scripts --- DONE